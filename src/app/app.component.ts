import { Component, HostListener } from '@angular/core';
import { StateAppService } from './state/state-app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  constructor(
    public stateAppService: StateAppService
  ) {
    
  }

  @HostListener('window:beforeunload', ['$event'])
    unloadNotification($event: any) {        
        let value = confirm('โปรดบันทึกข้อมูลก่อนออกจากเว็ปไซต์')
        if (value) {
            return true
        } else {
            return false
        }
    }
}
