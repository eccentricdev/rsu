import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'src/app/shared/models/subject';
import { StudentProfile } from 'src/app/shared/models/profile-student';

@Component({
  selector: 'app-list-student',
  templateUrl: './list-student.component.html',
  styleUrls: ['./list-student.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListStudentComponent implements OnInit {
  @Input() searchResult$:Observable<StudentProfile>
  @Output() onSearchStudent = new EventEmitter<string>()
  @Output() onSelectStudent = new EventEmitter<StudentProfile>()
  @ViewChild('search') inputSearch: ElementRef<HTMLInputElement>
  constructor() { }

  ngOnInit(): void {
  }

  searchStudent(){
    let studentCode = this.inputSearch.nativeElement.value
    this.onSearchStudent.emit(studentCode)
  }

  selectStudent(student: StudentProfile){
    this.onSelectStudent.emit(student)
  }
}
