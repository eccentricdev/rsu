import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginAsStudentContainer } from './container/login-as-student/login-as-student.container';
import { ListStudentComponent } from './presentational/list-student/list-student.component';
import { Routes, RouterModule } from '@angular/router';

const route:Routes = [
  {
    path:'',
    component:LoginAsStudentContainer
  }
]


@NgModule({
  declarations: [
    LoginAsStudentContainer, 
    ListStudentComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route)
  ]
})
export class LoginAsStudentModule { }
