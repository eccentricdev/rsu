import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'src/app/shared/models/subject';
import { StudentInformationService } from 'src/app/core/services/loginas/student-information.service';
import { StudentProfile } from 'src/app/shared/models/profile-student';
import { tap } from 'rxjs/operators';
import { LoginasService } from 'src/app/core/services/loginas/loginas.service';
import { StateAppService } from 'src/app/state/state-app.service';
import { StudentProfileService } from 'src/app/core/services/student-profile.service';
import { ProfileStudentStateService } from 'src/app/state/profile-student-state.service';

@Component({
  selector: 'app-login-as-student',
  templateUrl: './login-as-student.container.html',
  styleUrls: ['./login-as-student.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginAsStudentContainer implements OnInit {
  public searchResult$: Observable<StudentProfile>
  
  constructor(
    private studentInformationSV: StudentInformationService,
    private loginAsStudentSV :LoginasService,
  ) { }

  ngOnInit(): void {
  }

  searchStudent(studentCode: string){
    this.searchResult$ = this.studentInformationSV.searchStudent(studentCode).pipe(
      tap(x => console.log(x))
    )
  }


  loginAsStudent(student: StudentProfile){
    this.loginAsStudentSV.loginAsStudent(student).pipe(      
      tap(x => console.log(x))
    ).subscribe()
  }
  
}
