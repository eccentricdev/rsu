import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlipWithOptionComponent } from './slip-with-option/slip-with-option.component';
import {  Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

const router:Routes = [
  {
    path:'',
    component:SlipWithOptionComponent
  }
]


@NgModule({
  declarations: [
    SlipWithOptionComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(router)
  ]
})
export class SlipwithoptionModule { }
