import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { PaymentPrecheckService, paymentPrecheck } from 'src/app/core/services/payment/payment-precheck.service';
import { catchError, tap } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import swal from 'sweetalert2'
import { StateCommitService } from 'src/app/state/commit-state.service';
import { Commit } from 'src/app/shared/models/commit';
import { AppService } from 'src/app/app.service';
import { splitMessage } from 'src/app/core/util/util';
@Component({
  selector: 'app-slip-with-option',
  templateUrl: './slip-with-option.component.html',
  styleUrls: ['./slip-with-option.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlipWithOptionComponent implements OnInit {
  precheckPayment$: Observable<any>
  haveLastPayment: boolean = null
  precheckPayment: paymentPrecheck = null
  selectPaymentMSG: string = ''
  constructor(
    public router:Router,
    public PaymentPrecheckSV: PaymentPrecheckService,
    public stateCommitSV :StateCommitService,
    public appSV:AppService,
    private cdRef:ChangeDetectorRef
  ) { }

  ngOnInit(): void {    
    this.precheckPayment$ = this.PaymentPrecheckSV.getPreCheckPayment().pipe(
      tap(x => console.log(x)),
      tap(x => {
        this.selectPaymentMSG = x.paymentMethods.find(x => x.payment_method_code == 'THAIQR').message || '' 
      }), 
      tap(x => this.precheckPayment = x),
      tap(x => x.lastPayment ? this.haveLastPayment = true : this.haveLastPayment = false),
      catchError(err => {
        this.router.navigate(['/rsu/home'])
        swal.fire({
          icon: 'error', 
          html: splitMessage(err.error.message),
          // text: err.error.message,
        })
        return throwError(err)
      })
    )
  }
  
  

  openPopUp(){ 
    if (this.precheckPayment.lastPayment) {
      this.haveLastPayment = true
    } else {
      this.haveLastPayment = false
    }
    this.cdRef.detectChanges()
    let element = document.getElementById('modal')
    element.style.display = 'block'
    this.appSV.onClick()
    
  }

  changeMethodPayment(){
    this.haveLastPayment = false
    let element = document.getElementById('modal')
    element.style.display = 'block'
  }

  gotoSlip(){
    this.router.navigate(['/slip'])
  }

}
