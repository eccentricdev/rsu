import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, Data } from '@angular/router';
import { tap, filter, switchMap, startWith, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { StateAppService, StateApp } from 'src/app/state/state-app.service';
import { StudentProfileService } from 'src/app/core/services/student-profile.service';
import { ProfileStudentStateService } from 'src/app/state/profile-student-state.service';
import { StudentProfile } from 'src/app/shared/models/profile-student';
import { StateCommitService } from 'src/app/state/commit-state.service';
import { PrecheckStateService } from 'src/app/state/precheck-state.service';
import { LateCheckStateService } from 'src/app/state/late-check-state.service';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MenuComponent implements OnInit {
  topic$: Observable<Data>
  appState$: Observable<StateApp>
  profileStudent$: Observable<StudentProfile>
  isShowMenu:boolean = false
  isStudent:boolean = true

  constructor(
    public activeRoute: ActivatedRoute,
    public router: Router,
    public stateAppSV :StateAppService,
    private profileStudentService:StudentProfileService,
    public profileStudentState: ProfileStudentStateService,
    public statCommit:StateCommitService,
    public preChecStateSV: PrecheckStateService,
    public latePreCheckState: LateCheckStateService,
    public preCheckState: PrecheckStateService
  ) { }

  ngOnInit(): void {
    this.topic$ = this.router.events.pipe(
      filter(x => x instanceof NavigationEnd),      
      startWith(''),
      filter(x => this.activeRoute.children.length != 0),
      switchMap(x => this.activeRoute.children[0].data), 
    )

    this.appState$ = this.stateAppSV.getStateChange().pipe(
      tap(x => console.log(x))
    )
    
    this.profileStudentService.getStudentProfile().pipe(
    ).subscribe()

    this.profileStudent$ = this.profileStudentState.getStudentProfileChange().pipe(
      tap(x => console.log(x)),
      tap(x => this.isStudent = this.stateAppSV.isStudent())
    )
  }


  logout(){
      this.stateAppSV.stateChangeLogout()
      this.statCommit.nexStateLogout()
      this.profileStudentState.nextStateLogout()
      this.preChecStateSV.logoutState()
      this.latePreCheckState.logoutState(),
      this.preCheckState.logoutState()
      this.router.navigate([''])
  }
}
