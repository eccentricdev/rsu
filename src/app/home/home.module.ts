import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeContainer } from './home/home.container';
import { MenuComponent } from './home/menu/menu.component';
import { RouterModule, Routes } from '@angular/router';

const routes:Routes = [
  {
    path:'app',
    component:HomeContainer,
  }
]


@NgModule({
  declarations: [
    HomeContainer,
    MenuComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    HomeContainer,
    MenuComponent
  ]
})
export class HomeModule { }
