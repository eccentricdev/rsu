import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/core/auth/auth.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  @ViewChild('username') userName: ElementRef<HTMLInputElement>
  @ViewChild('password') password: ElementRef<HTMLInputElement>
  isSelectThai: boolean = true
   
  constructor(
    public authSV :AuthService,
  ) { }

  ngOnInit(): void {
      
  }

  login(){
    let userName = this.userName.nativeElement.value + ''
    let password = this.password.nativeElement.value
    console.log(environment.production)
    if (!environment.production) {
      // 6000036
        // 6103545
      // userName = '6102721'
      password = '0123456789'

      // userName = '5905849'
      // password = '0123456789'
      
      // userName = '5900324'
      // password = '0123456789'
      
      // userName = '6103545'
      // password = '0123456789'

      // userName = '6100621'
      // password = '0123456789'
      
      // userName = '6100122'
      // password = '0123456789'

      // userName = '6001575'
      // password = '0123456789'

      // userName = '6204550'
      // password = '0123456789'

      // userName = '6000377'
      // password = '0123456789'

      // userName = '6204363'
      // password = '0123456789'

      // userName = '6204764'
      // password = '0123456789'

      //law797
      // 6204550
      // userName = '6204550',
      // password = '0123456789'

      // userName = '6204702',
      // password = '0123456789'

      // userName = '6204471',
      // password = '0123456789'

      // user
      // userName = '6204702',
      // password = '0123456789'
      
      // user here
      // userName = '6204550',
      // password = '0123456789'

      // user
      // userName = '6000585',
      // password = '1100280013474'
      // userName = "6000595",
	    // password = "1769900500913"
      // userName = "6204764",
      // password = "0123456789"
      
      // emp
      // userName = '2690140'
      // password = '0123456789'
      
    }
    
    
    this.authSV.login({
      loginName: userName,
      password: password,          
      language: this.isSelectThai ? "TH": "EN"
    }).pipe(
    ).subscribe()
  }


  clearForm(){
    this.userName.nativeElement.value = ''
    this.password.nativeElement.value = ''
  }
}
