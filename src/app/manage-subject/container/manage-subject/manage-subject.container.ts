import { Component, OnInit, ChangeDetectionStrategy, ComponentFactoryResolver } from '@angular/core';
import { PrecheckStateService } from 'src/app/state/precheck-state.service';
import { Observable, from } from 'rxjs';
import { PreCheck, SelectedSubject } from 'src/app/shared/models/precheck';
import { PrecheckService } from 'src/app/core/services/precheck.service';
import { tap, concatMap, finalize } from 'rxjs/operators';
import { Subject, AvailableLectureGroup } from 'src/app/shared/models/subject';
import { SubjectService } from 'src/app/core/services/subject.service';
import { LateSubjectService } from 'src/app/core/services/late-subject.service';
import { LatePrecheckService } from 'src/app/core/services/late-precheck.service';
import { ActivatedRoute } from '@angular/router';
import { PreCommitService } from 'src/app/core/services/pre-commit.service';
import { LateCommitService } from 'src/app/core/services/late-commit.service';
import { StateCommitService } from 'src/app/state/commit-state.service';
import swal from 'sweetalert2'
import { ProfileStudentStateService } from 'src/app/state/profile-student-state.service';
import { StudentProfile } from 'src/app/shared/models/profile-student';
import { splitMessage } from 'src/app/core/util/util';

@Component({
  selector: 'app-manage-subject',
  templateUrl: './manage-subject.container.html',
  styleUrls: ['./manage-subject.container.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ManageSubjectContainer implements OnInit {
  public statePrecheck$: Observable<PreCheck>
  searchResult$: Observable<Subject>
  private searchResult:Subject
  state:string = 'pre'
  addListSubjects: Subject[] = []
  watingListSubjects: Subject[] = []
  deleteListSubject: Subject[] = []
  profileStudent: StudentProfile
  selectedSubject: SelectedSubject[] = []


  constructor(
    public precheckSV: PrecheckService,
    private statePrecheck: PrecheckStateService,
    public subjectSV: SubjectService,
    public lateSubjectSV:LateSubjectService,    
    public latePrecheck: LatePrecheckService,    
    public activeRoute: ActivatedRoute,
    public preCommitSV: PreCommitService,
    public lateCommitSV: LateCommitService,
    public stateCommitSV :StateCommitService,
    public profileState: ProfileStudentStateService
  ) { 
    this.profileStudent = this.profileState.getStudentProfile()
  }

  ngOnInit(): void {    
    this.getPreheck().pipe(tap(x => console.log(x))).subscribe()
    this.statePrecheck$ =  this.statePrecheck.getStateChange().pipe(
      tap(x => console.log(x)),
      tap(x => this.selectedSubject = [...x.selectedSubjects]),
      tap( x => {
        let waitingList  = x.selectedSubjects.filter(subject => subject.status !== "NORMAL")
        this.watingListSubjects = [...waitingList] as Subject[]          
    })
    )
  }

  getPreheck(){
    return this.precheckSV.getPreCheck()
  }

  openPopup(){
    let element = document.getElementById('dynamicmodal')
    element.style.display = 'block'
  }

   


  private SelectSubject(){
      from(this.addListSubjects).pipe(
        concatMap( x => {  
          return  this.subjectSV.addSubject(x)
        }),
      ).subscribe(
        _ => {},
        _ => {},
        () => {
          this.addListSubjects  = [...[]]
          this.precheckSV.getPreCheck().pipe(
            tap(x => swal.fire(
              'ยื่นยันรายการเรียบร้อยแล้ว',
              'success'
            ))
          ).subscribe()
        }
      )
  }


  searchSubject(subjectCode:string){      
    if (this.state == 'pre') {
        this.searchResult$ = this.subjectSV.searchSubject(`${subjectCode}`).pipe(
          tap(x => this.searchResult = x),
          tap(x => this.openPopup())
        )  
    } else {
      this.searchResult$ = this.lateSubjectSV.searchSubject(`${subjectCode}`).pipe(
        tap(x => this.searchResult = x),
        tap(x => this.openPopup())
      )  
    }    
  }

  selectSubject(subject: AvailableLectureGroup){
    let newResult = {...this.searchResult,selectedLectureGroup:subject}
    if(this.isDuplicateSubject(newResult)) return this.swalAlert()    
    delete newResult.availableLectureGroups  
    let listSubject = [...this.addListSubjects,newResult]    
    this.addListSubjects = [...listSubject]
    console.log(this.addListSubjects)
    this.searchResult$ = null
    let element = document.getElementById('dynamicmodal')
    element.style.display = 'none'
  }

  addSubjectToDelete(subject: Subject){
    let index = this.selectedSubject.findIndex(sub => subject.subjectCode == sub.subjectCode)
    this.selectedSubject.splice(index,1)
    let newarray = [...this.selectedSubject]
    this.selectedSubject = [...newarray]
    
    let arrayDelete = [...this.deleteListSubject,subject]
    this.deleteListSubject = [...arrayDelete]
  } 

  
  deleteSelect(subject: Subject){
    let newResult = {...this.searchResult,selectedLectureGroup:subject}    
    let index  = this.addListSubjects.findIndex(item => newResult.subjectCode == item.subjectCode)
    this.addListSubjects.splice(index,1)
    let newList  = [...this.addListSubjects]
    this.addListSubjects = [...newList]
  }

  deleteSubjectIntentToRemove(subject: Subject){
    let index  = this.deleteListSubject.findIndex(item => subject.subjectCode == item.subjectCode)    
    this.deleteListSubject.splice(index,1)    
    let newList  = [...this.deleteListSubject]
    this.deleteListSubject = [...newList]

    let newArray = [...this.selectedSubject,subject]
    this.selectedSubject = [...newArray]
    console.log(subject)

  }

  confirmAddSubject(event){
    swal.fire({
      title: 'Are you sure?',
      text: "You need to change subject",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sure !'
    }).then((result) => {
      if (result.value) {
        this.SelectSubject()
      }
    })
  }

  confirmDeleteSubject(event){

    if (this.addListSubjects.length == 0 && this.deleteListSubject.length == 0) {
        return swal.fire(
          'โปรดเลือกวิชาที่ต้องการดำเนินการ',
        )
    }

    swal.fire({
      title: 'Are you sure?',
      // text: "You need to remove subject",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sure !'
    }).then((result) => {
      if (result.value) {
        if (this.addListSubjects.length != 0) {
          this.SelectSubject()  
        }
        if (this.deleteListSubject.length != 0) {
          this.deleteSubjectFromServer()
        }                
      }
    })
  }

  private deleteSubjectFromServer(){
    from(this.deleteListSubject).pipe(
      concatMap(x => {
        return this.subjectSV.deleteSubject(x)
      })
    ).subscribe(
      (x) => {},
      (err) => {},
      () => {
        this.deleteListSubject  = [...[]]
        this.precheckSV.getPreCheck().pipe(
            tap(x => swal.fire(
              'ยื่นยันรายการเรียบร้อยแล้ว',
              'success'
            ))
        ).subscribe()
      }
    )
  }

  isDuplicateSubject(subject: Subject){
    return this.statePrecheck.getValue().selectedSubjects.some(x => x.subjectCode == subject.subjectCode)
  }

  swalAlert(){
    swal.fire({
      icon: 'error',
      // text: "Look like your subject have time period in your Subject",
      text: "You Already have this subject",
    })
  }

  swalAlertTiem(){
    swal.fire({
      icon: 'error',
      text: "Look like your subject have time period in your Subject",      
    })
  }
  swalAlertItem(text = 'error'){
    swal.fire({
      icon: 'error',
      html: splitMessage(text),
    })
  }
}
