import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { Subject } from 'src/app/shared/models/subject';
import { StudentProfile } from 'src/app/shared/models/profile-student';

@Component({
  selector: 'app-waiting-list',
  templateUrl: './waiting-list.component.html',
  styleUrls: ['./waiting-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WaitingListComponent implements OnInit {
  @Input() isReadOnly: boolean
  @Input() subjects = []
  @Input() profileStudent: StudentProfile
  constructor() { }

  ngOnInit(): void {
  }

}
