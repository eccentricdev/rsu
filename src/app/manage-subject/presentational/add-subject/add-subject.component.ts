import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter, ViewChild, ElementRef, Input } from '@angular/core';
import { Subject, AvailableLectureGroup } from 'src/app/shared/models/subject';
import { Observable } from 'rxjs';
import { StudentProfile } from 'src/app/shared/models/profile-student';

@Component({
  selector: 'app-add-subject',
  templateUrl: './add-subject.component.html',
  styleUrls: ['./add-subject.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddSubjectComponent implements OnInit {
  @Input() isReadOnly: boolean
  @Output() onDeleteSubject  = new EventEmitter<Subject>()
  @Output() onCommit = new EventEmitter<void>()
  @Output() onSearchSubject = new EventEmitter<string>()
  @Output() onSelectSubject = new EventEmitter()
  @Input() searchResult$:Observable<Subject>
  @Input() listSubject  = []
  @Input() profileStudent: StudentProfile
  @ViewChild('search') private inputSearchSubject:ElementRef<HTMLInputElement>
  
  constructor() { }

  ngOnInit(): void {
  }

  deleteSubject(subject :Subject){
    this.onDeleteSubject.emit(subject)
  }

  commit(){
    this.onCommit.next()
  }

  searchSubject(){
    let subjectCode:string = this.inputSearchSubject.nativeElement.value
    
    this.onSearchSubject.emit(subjectCode)
  }

  selectSubject(subject: AvailableLectureGroup){
    this.searchResult$ = null    
    this.onSelectSubject.emit(subject)
  }

}
