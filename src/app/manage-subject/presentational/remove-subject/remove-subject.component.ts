import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'src/app/shared/models/subject';
import { StudentProfile } from 'src/app/shared/models/profile-student';

@Component({
  selector: 'app-remove-subject',
  templateUrl: './remove-subject.component.html',
  styleUrls: ['./remove-subject.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RemoveSubjectComponent implements OnInit {
  @Input() isReadOnly: boolean
  @Input() subjects  = []
  @Output() onCommit = new EventEmitter<void>()
  @Input() profileStudent: StudentProfile
  @Output() onDeleteSubject =  new EventEmitter<Subject>()
  constructor() { }

  ngOnInit(): void {
  }

  deleteSubject(subject:Subject){
    this.onDeleteSubject.emit(subject)
  }
  
  onClickCommit(){
    this.onCommit.emit()
  }

}
