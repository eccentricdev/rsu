import { Subject } from './../../../shared/models/subject';
import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { PreCheck, SelectedSubject } from 'src/app/shared/models/precheck';
import { StudentProfile } from 'src/app/shared/models/profile-student';


@Component({
  selector: 'app-list-manage-subject',
  templateUrl: './list-manage-subject.component.html',
  styleUrls: ['./list-manage-subject.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class ListManageSubjectComponent implements OnInit {
  @Input() selectSubjects: SelectedSubject
  @Input() isReadOnly: boolean
  @Input() profileStudent: StudentProfile
  @Output() onDeleteSubject = new EventEmitter<Subject>()

  constructor() { }

  ngOnInit(): void {
  }

  deleteSubject(subject :Subject){
    this.onDeleteSubject.emit(subject)
  }

}
