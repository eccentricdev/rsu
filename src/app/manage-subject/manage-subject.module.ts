import { SharedModule } from './../shared/shared.module';
import { Routes, Router, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageSubjectContainer } from './container/manage-subject/manage-subject.container';
import { ListManageSubjectComponent } from './presentational/list-manage-subject/list-manage-subject.component';
import { AddSubjectComponent } from './presentational/add-subject/add-subject.component';
import { RemoveSubjectComponent } from './presentational/remove-subject/remove-subject.component';
import { WaitingListComponent } from './presentational/waiting-list/waiting-list.component';

const route:Routes = [
  {
    path:'',
    component:ManageSubjectContainer
  }
]

@NgModule({
  declarations: [
    ManageSubjectContainer, 
    ListManageSubjectComponent, 
    AddSubjectComponent, 
    RemoveSubjectComponent, 
    WaitingListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    SharedModule
    
  ]
})
export class ManageSubjectModule { }
