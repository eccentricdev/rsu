import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalComponent } from './components/modal/modal.component';
import { FormsModule } from '@angular/forms';
import { QRCodeModule } from 'angularx-qrcode';
import { ThaibathPipe } from './pipe/thaibath.pipe';
import { DynamicModalComponent } from './components/dynamic-modal/dynamic-modal/dynamic-modal.component';
import { DisabledCheckboxDynamicPopupPipe } from './pipe/disabled-checkbox-dynamic-popup.pipe';
import { TypeOnlyNumberDirective } from './directives/type-only-number.directive';


@NgModule({
  declarations: [
    ModalComponent,
    ThaibathPipe,
    DynamicModalComponent,
    DisabledCheckboxDynamicPopupPipe,
    // TypeOnlyNumberDirective
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,    
    QRCodeModule
  ],
  exports:[
    ReactiveFormsModule,
    ModalComponent,
    DynamicModalComponent,
    FormsModule,
    QRCodeModule,
    ThaibathPipe,
    DisabledCheckboxDynamicPopupPipe,
    // TypeOnlyNumberDirective
  ]
})
export class SharedModule { }
