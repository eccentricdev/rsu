import { Menu } from 'src/app/state/state-app.service';

export interface ResultLogin {
    name: string;
    asName: string
    refreshToken: string;
    token: string;
    tokenExpiryDate: string;
    roles: any[];
    menus: Menu[];
}