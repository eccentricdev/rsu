export interface User {
    loginName: string;
    password: string;
    language: string;
  }