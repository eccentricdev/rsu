export interface Commit {
  totalCredit: number;
  creditFee: number;
  academicFee: number;
  accidentInsuranceFee: number;
  fundAmount: number;
  loanAmount: number;
  advanceAmount: number;
  package: number;
  totalPaymentAmount: number;
  payInNo: string;
  academics: Academic[];
  fund: Fund;
  loan?: any;
  subjects: Subject[];
  credits: Credit[];
  debits: Credit[];
  total: Credit;
  payEnable: boolean;
  isCommit: boolean;
}

export interface Credit {
  name: string;
  amount: number;
}

export interface Subject {
  subjectCode: string;
  subjectName: string;
  credit: number;
  section: string;
  lectureAmount: number;
  labAmount: number;
  totalAmount: number;
  status?: any;
}

export interface Fund {
  scholarshipCode: string;
  scholarshipName: string;
}

export interface Academic {
  feeCode: string;
  feeName: string;
  amount: number;
}