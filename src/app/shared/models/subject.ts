export interface Subject {
    subjectSequence: number;
    subjectCode: string;
    subjectNameTh: string;
    subjectNameEn: string;
    credit: number;
    labAmount: number;
    lectureAmount: number;
    isSpecialTopic: string;
    status: string;
    availableLectureGroups: AvailableLectureGroup[];
    availableLabGroups?: any;
    availableTopics?: any;
  }
  
  export interface AvailableLectureGroup {
    sectionId: string;
    quota: number;
    isFull: boolean
    confirmSeat?: number;
    reserveSeat: number;
    studyDay: string;
    studyStartTime: string;
    studyEndTime: string;
    lectureMidtermDate: string;
    lectureFinalDate: string;
    lectureMidtermStartTime: string;
    lectureMidtermEndTime: string;
    lectureFinalStartTime: string;
    lectureFinalEndTime: string;
    labMidtermDate?: any;
    labFinalDate?: any;
    labMidtermStartTime?: any;
    labMidtermEndTime?: any;
    labFinalStartTime?: any;
    labFinalEndTime?: any;
    studyTime?: string;
    topicCode: string
  }