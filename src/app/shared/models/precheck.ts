export interface PreCheck {
    minCredit: number;
    maxCredit: number;
    academicYear: number;
    academicSemester: number;
    isEligibleToEnroll: boolean;
    message: string;
    isReadOnly: boolean
    currentCredit: number
    selectedSubjects: SelectedSubject[];
    totalPaymentAmount: number,
    credits: credit[],
    debits: credit[],
  }

export interface credit {
  amount:number,
  name: string
}
  
export interface SelectedSubject {
    subjectSequence: number;
    subjectCode: string;
    subjectNameTh: string;
    subjectNameEn: string;
    credit: number;
    labAmount: number;
    lectureAmount: number;
    isSpecialTopic: string;
    status: string;
    flag?: string
    selectedLectureGroup?: any;
    selectedLabGroup?: any;
    selectedTopic?: any;
  }