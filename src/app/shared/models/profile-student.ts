export interface StudentProfile {
  studentCode: string;
  studentName: string;
  educationType: string;
  educationTypeName: string;
  faculty: string;
  facultyName: string;
  major: string;
  majorName: string;
  email: string;
  status: string;
  statusName: string;
  academicYearEntry: string;
  academicSemeterEntry: string;
  packageStatus: string;
  gpa: number;
  credit: number;
  studentYear: number;
  advisor: string;
  advisorPhone: string;
  advisorEmail: string;
}