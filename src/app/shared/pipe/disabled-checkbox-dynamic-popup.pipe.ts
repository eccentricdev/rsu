import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'disabledCheckboxDynamicPopup',
  pure: true
})
export class DisabledCheckboxDynamicPopupPipe implements PipeTransform {

  transform(array: [] ,data:any, key:string): boolean {
   
    if (array) {
      let isSelect = array.some(x => x[key] == data[key])
      return isSelect;  
    }
    return false
    
  }

}
