import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import {  Observable } from 'rxjs';
import {  Subject } from 'src/app/shared/models/subject';
import swal from 'sweetalert2'
import { PrecheckStateService } from 'src/app/state/precheck-state.service';
import { tap, take, map } from 'rxjs/operators';
@Component({
  selector: 'app-dynamic-modal',
  templateUrl: './dynamic-modal.component.html',
  styleUrls: ['./dynamic-modal.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class DynamicModalComponent implements OnInit,OnChanges {
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    if (this.searchResult$) {
      this.condition = []
      this.clearRequest()

      this.searchResult$.pipe(
        take(1),
        tap(x => {
          this.searchResult = x
          console.log(x)
          if(x.isSpecialTopic == "Y"){
              this.mergeData()
          }
          this.condition = []
          if (x.availableLectureGroups) {
            this.condition.push('availableLectureGroups')
          }
          if (x.availableLabGroups) {
            this.condition.push('availableLabGroups')
          }
          if (x.availableTopics) {
            this.condition.push('availableTopics')
          }

          let array  = [...this.condition]
          this.condition = [...array]
        }),
        // map(data => {
        //   if(data.isSpecialTopic == "Y") {
        //     let newData = data.availableTopics.map((el,index,self:[]) => {
        //       let selectData = data.availableLectureGroups.find(lg => lg.topicCode == el.topicCode)
        //       return {
        //         ...el,
        //         ...selectData
        //       }
        //     })
        //     data.availableTopics = [...newData]
        //     return data
        //   } else {
        //     return data
        //   }
        // }),
        // tap(x => console.log(x))
      ).subscribe()  
    }    
  }
  @Input() header: string = ''
  @Input() listSubject: Subject[] = []
  @Input() searchResult$: Observable<Subject>
  @Output() onSelectSubject = new EventEmitter<any>()
  condition = []
  countSelectSubject = []
  studyTime = 'THU (13:00-15:50) , THU (13:00-15:50)'

  public searchResult: Subject
  

  public request = {
    selectedLectureGroup: null,
    selectedLabGroup: null,
    selectedTopic: null
  }

  constructor(
    public precheckState: PrecheckStateService
  ) { }

  ngOnInit(): void {

  }

  private mergeData(){
    let newData = this.searchResult.availableTopics.map((x,index,self:[]) => {
      let selectData = this.searchResult.availableLectureGroups.find(lg => lg.topicCode == x.topicCode)
      return {
        ...x,
        ...selectData
      }
    })
    this.searchResult.availableTopics = newData
    console.log(this.searchResult)
  }


  closePopUp(){    
    let element = document.getElementById('dynamicmodal')
    element.style.display = 'none'
    this.countSelectSubject = []
    this.condition = []
    this.clearRequest()
  }

  clickLectureGroups(html: HTMLInputElement,subject) {
    if (!html.checked) {
       html.checked = true
    }
    this.selectLectureGroups(html.checked,subject)
  }

  clickselectLap(html: HTMLInputElement,subject){
    console.log(html)
    console.log(subject)
  // if (!html.checked) {
      html.checked = true
  //  }
    this.request.selectedLabGroup = subject
    this.disabledSave()
    this.selectLap(html.checked,subject)
  }


  clickSelectTopic(html: HTMLInputElement,subject){
    if (!html.checked) {
      html.checked = true
   }
   
   if (this.searchResult.isSpecialTopic == 'Y') {
     let lecture = this.searchResult
                          .availableLectureGroups
                          .find(lecture => lecture.topicCode == subject.topicCode)
      let input = document.getElementById(lecture.topicCode) as HTMLInputElement
      input.checked = true
      this.selectLectureGroups(true,lecture)
   }

   this.selectTopic(html.checked,subject)
  }

  selectSubject(){  
    if (this.request.selectedLectureGroup?.isFull || this.request.selectedLabGroup?.isFull  || this.request.selectedTopic?.isFull) {
      swal.fire({
        title: 'วิชาที่เลือกจะไปอยู่ในรายการ waitinglist ต้องการดำเนินการต่อหรือไม่',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText:'ยกเลิก',
        confirmButtonText: 'ยืนยัน'
      }).then((result) => {
        if (result.value) { 
          this.onSelectSubject.emit(this.request)
          this.condition = []
          this.clearRequest()
          this.countSelectSubject = []
        }
      })
    } else {
      this.onSelectSubject.emit(this.request)
      // this.condition = []    
      // this.clearRequest()
      // this.countSelectSubject = []
    }    
  }

  clearRequest(){
    this.request.selectedLabGroup = null
    this.request.selectedLectureGroup = null
    this.request.selectedTopic = null
  }

   selectLectureGroups(check,subject){     
     
     if (check) {        
        this.request.selectedLectureGroup = subject        
     } else {
       this.request.selectedLectureGroup = null
     }
     this.disabledSave()
   }

   selectLap(check,subject){    
      if (check) {
        this.request.selectedLabGroup = subject
      } else {
        this.request.selectedLabGroup = null
      }
      this.disabledSave()
   }

   selectTopic(check,subject){    
      if (check) {
        this.request.selectedTopic = subject
      } else {
        this.request.selectedTopic = null
      }
      this.disabledSave()
   }


   disabledSave(){
     this.countSelectSubject = []
     Object.keys(this.request).map(key  =>  {
       if (this.request[key] != null) {
          this.countSelectSubject.push(this.request[key])
       }
     })
   }

   handleIsSpecialTop(result: Subject){
     if (result.isSpecialTopic == 'Y') return 'special_topic'
     if (result.isSpecialTopic != 'Y') return ''
   }
}
