import { paymentPrecheck, PaymentMethod } from './../../../core/services/payment/payment-precheck.service';
import { tap, withLatestFrom, takeUntil, catchError } from 'rxjs/operators';
import { Component, OnInit, Input, OnDestroy, ChangeDetectorRef, OnChanges } from '@angular/core';
import { AlipayService } from 'src/app/core/services/payment/alipay.service';
import { PromptpayService } from 'src/app/core/services/payment/promptpay.service';
import { WechatService } from 'src/app/core/services/payment/wechat.service';
import swal from 'sweetalert2'
import { VisaService } from 'src/app/core/services/payment/visa.service';
import { StateCommitService } from 'src/app/state/commit-state.service';
import { Subscription, Subject, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { AppService } from 'src/app/app.service';
import { splitMessage } from 'src/app/core/util/util';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ModalComponent implements OnInit,OnDestroy,OnChanges {  
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    console.log(changes)
    // let ref1 = document.getElementById('Ref1') as HTMLInputElement
    // ref1.value = changes.redf1
  }
  
  ngOnDestroy(): void {
    this.openFrom = ''
    this.destroy$.next(true)
    this.destroy$.complete()    
  }

  @Input() openFrom: string = ''  
  @Input() haveLastPayment: boolean = null
  @Input() paymentPrecheck: paymentPrecheck
  @Input() message: string = ''
  fee: number = 0
  imageBase64: string
  private destroy$ = new Subject()
  selectPaymentIndex: number = -1
  selectPayment: PaymentMethod = null
  constructor(
    public alipaySV : AlipayService,
    public promptPaySV: PromptpayService,
    public weChatSV :WechatService,
    public visaSV: VisaService,
    public http: HttpClient,
    public cdRef:ChangeDetectorRef,
    public appSV:AppService,
    public router: Router
  ) { }

  ngOnInit(): void {    
    this.appSV.onDialogOpenChange().pipe(
      tap(x =>  {
        console.log(x)
        let element = document.getElementById('modal')
        console.log('click')
        if (this.openFrom === 'slip' && this.haveLastPayment && element.style.display == 'block') {
          console.log('call api')
          this.http.get(`${this.paymentPrecheck.lastPayment.request_url}`).pipe(
            catchError(err => {
              swal.fire({
                icon: 'error',
                html: splitMessage(err.error.message),
                // text: err.error.message,
              })
              return throwError(err)
            })
          ).subscribe( (x:any) => {
            console.log(x)
            let form:any = document.getElementById('form1')
            let ref1 = document.getElementById('Ref1') as HTMLInputElement
            let amount = document.getElementById('Amount') as HTMLInputElement
            let ref2 = document.getElementById('Ref2') as HTMLInputElement
            ref1.value = x.ref1
            ref2.value = x.ref2
            amount.value = x.amount
            form.action = x.baseUrl
            if (this.paymentPrecheck.lastPayment.payment_method_code == "BILL") {
                return 
            }

            if (x.imageURL) {
                let win = window.open(x.imageURL,'_blank')
            }
            if (x.image) {
                let prefix = 'data:image/jpeg;base64,'
                this.imageBase64 =`${prefix}${x.image}`
                swal.fire({
                  html:
                    `<img style="width: 200px;height: 200px;" src="${this.imageBase64}" alt="">
                      <p>${this.message || 'no message'}</p>
                    `,
                  showConfirmButton: false,
                })
            }
            if (x.fullUrl) {
              // let win = window.open(x.fullUrl,'_blank')
              form.submit()
            }

            // if (!x.imageURL && !x.image && !x.qr && !x.type) {
            //   swal.fire({
            //     icon: 'error',
            //     text: 'No data',
            //   })
            // }
            this.cdRef.detectChanges()
        })}
      }),
      takeUntil(this.destroy$)
    ).subscribe()
  }

  closePopUp(){
    let element = document.getElementById('modal')
    element.style.display = 'none'    
  }

  submitForm(){
    
    let form:any = document.getElementById('form1')
    // form.action  = this.selectPayment.api_url
    form.submit();
  }

  changePaymentMethod(){
    swal.fire({
      title: 'ต้องการที่จะเปลี่ยนวิธีการชำระเงินหรือไม่ ?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ยืนยัน',
      cancelButtonText:'ยกเลิก'      
    }).then((result) => {
      if (result.value) {
        this.haveLastPayment = !this.haveLastPayment
        this.cdRef.detectChanges()
      }
    })
  }

  gotoSlip(){
    // alert('slip')
    this.router.navigate(['/slip'])
  }

  selectPaymentMethod(index: number, payment: PaymentMethod){
    this.selectPaymentIndex = index
    this.selectPayment = payment
    console.log(this.selectPayment)
    this.fee = payment.fee
  }

  onClickPaymentByLastPayment(payment: PaymentMethod){
    swal.queue([{
      title: `Are you sure to payment with ${payment.payment_method_name} ?`,
      confirmButtonText: 'Payment',
      onAfterClose: () => {
        this.fee = 0
        this.cdRef
      },
      showLoaderOnConfirm: true,      
      preConfirm: () => {
        if (this.selectPayment.payment_method_code == 'CREDITCARD') {
          // return this.submitForm()
          return this.http.get(`${payment.api_url}`).toPromise().then((x:any) => {
            let form:any = document.getElementById('form1')
            let ref1 = document.getElementById('Ref1') as HTMLInputElement
            let amount = document.getElementById('Amount') as HTMLInputElement
            let ref2 = document.getElementById('Ref2') as HTMLInputElement
            ref1.value = x.ref1
            ref2.value = x.ref2
            amount.value = x.amount
            form.action  = x.baseUrl
            form.submit();
          }).catch(err => {
            swal.fire({
              icon: 'error',
              html: splitMessage(err.error.message),
              // text: err.error.message,
            })
          })
        }

        return this.http.get(`${payment.api_url}`).toPromise().then( (x:any) => {
            if (x.imageURL) {
                let win = window.open(x.imageURL,'_blank')
            }
            if (x.image) {
                let prefix = 'data:image/jpeg;base64,'
                this.imageBase64 =`${prefix}${x.image}`
                swal.fire({
                  html:
                    `<img style="width: 200px;height: 200px;" src="${this.imageBase64}" alt="">
                      <p>${this.selectPayment.message}</p>
                    `,
                  showConfirmButton: false,
                })
            }
            if (x.fullUrl) {
              let win = window.open(x.fullUrl,'_blank')
            }
            this.cdRef.detectChanges()
            
        }).catch(err => {
          swal.fire({
            icon: 'error',
            html: splitMessage(err.error.message),
            // text: err.error.message,
          })
        })
      }
    }])

  }

}
