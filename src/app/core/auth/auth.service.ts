import { Injectable } from '@angular/core';
import { HttpClient, HttpBackend } from '@angular/common/http';
import { User } from 'src/app/shared/models/user';
import { BaseService } from '../base/base-service';
import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import swal from 'sweetalert2'
import { Router } from '@angular/router';
import { StateAppService, Menu } from 'src/app/state/state-app.service';
import { StudentProfileService } from '../services/student-profile.service';
import {  splitMessage } from '../util/util';
export interface StatusUser {
  name: string;
  asName:string,
  refreshToken: string;
  token: string;
  tokenExpiryDate: string;
  roles: any[];
  menus: Menu[];
}

@Injectable({
  providedIn: 'root'
})

export class AuthService extends BaseService {
  
  constructor(
    public http :HttpClient,
    public handler :HttpBackend,
    public router: Router,
    public appState: StateAppService,
  ) { 
    super('/security/login')
    this.http = new HttpClient(handler)     
  }

  login(user:User):Observable<StatusUser> {
    let user$ = this.http.post<StatusUser>(this.fullUrl,user).pipe(
      tap(x => console.log(x)),
      tap(x => this.appState.stateChangeLogin(x)),
      tap(x => this.navigateByRole()),
      catchError(err => {
        swal.fire({
          icon: 'error',
          text: "ไม่สามารถเข้าสู่ระบบได้ โปรดลองอีกครั้ง"
        })
        return throwError(err)
      })
    )
    
    return user$
  }

  private navigateByRole(){
      if (this.appState.getValue().roles.includes("STUDENT")) {
          this.router.navigate(['/rsu/home'])
      } else {
        this.router.navigate(['/rsu/select-student']) 
      }
  }

}
