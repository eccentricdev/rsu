import { Injectable } from '@angular/core';
import { BaseService } from '../base/base-service';
import { Observable, throwError, of } from 'rxjs';
import { StudentProfile } from 'src/app/shared/models/profile-student';
import { HttpClient } from '@angular/common/http';
import { ProfileStudentStateService } from 'src/app/state/profile-student-state.service';
import { tap, catchError } from 'rxjs/operators';
import { PrecheckService } from './precheck.service';
import { LatePrecheckService } from './late-precheck.service';

@Injectable({
  providedIn: 'root'
})
export class StudentProfileService  extends BaseService{

  constructor(
    public http :HttpClient,
    public profileStudentState:ProfileStudentStateService,
    public precheck: PrecheckService,
    public latePrecheck: LatePrecheckService
  ) { 
    super('/student/profile')
  }

    getStudentProfile():Observable<StudentProfile>{
      let profileStudent$ = this.http.get<StudentProfile>(this.fullUrl).pipe(
        tap(x => this.profileStudentState.nextStateStudentProfile(x)),
        tap(x => console.log('data from service',x)),
        tap(x => {
          this.precheck.getPreCheck().subscribe()
          this.latePrecheck.getPreCheck().subscribe()
        }),
        catchError(err => {
          this.profileStudentState.nextStateStudentProfile(null)
          return of(null)
        })        
      )      
      return profileStudent$
    }
}
