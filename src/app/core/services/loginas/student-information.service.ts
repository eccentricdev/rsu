import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../../base/base-service';
import { Observable } from 'rxjs';
import { StudentProfile } from 'src/app/shared/models/profile-student';

@Injectable({
  providedIn: 'root'
})
export class StudentInformationService extends BaseService {

  constructor(
    public http:HttpClient
  ) { 
    super('/security/student')
  }


    searchStudent(studentCode:string):Observable<StudentProfile>{
      return  this.http.get<StudentProfile>(`${this.fullUrl}/${studentCode}`)
    }
   
}
