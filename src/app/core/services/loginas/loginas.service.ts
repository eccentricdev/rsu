import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { HttpClient } from '@angular/common/http';
import { StateAppService } from 'src/app/state/state-app.service';
import { tap, concatMap } from 'rxjs/operators';
import { ResultLogin } from 'src/app/shared/models/resul-login';
import { StudentProfileService } from '../student-profile.service';
import { ProfileStudentStateService } from 'src/app/state/profile-student-state.service';
import { StudentProfile } from 'src/app/shared/models/profile-student';
import { forkJoin } from 'rxjs';
import { PrecheckService } from '../precheck.service';
import { LatePrecheckService } from '../late-precheck.service';

@Injectable({
  providedIn: 'root'
})
export class LoginasService extends BaseService {

  constructor(
    public http:HttpClient,
    private stateApp :StateAppService,
    private studentPrfileState: ProfileStudentStateService,
    private precheck:PrecheckService,
    private latePrecheck: LatePrecheckService
  ) { 
    super('/security/loginas')
  }



  loginAsStudent(student: StudentProfile){
    return this.http.get<ResultLogin>(`${this.fullUrl}/${student.studentCode}`).pipe(
      tap(x => console.log(x)),
      tap(x => this.stateApp.stateChangeLogin(x)),
      tap(x => this.studentPrfileState.nextStateStudentProfile(student)),
      concatMap(x => {
        return forkJoin([
          this.precheck.getPreCheck(),
          this.latePrecheck.getPreCheck()
        ])
      })
    )
  }


}
