import { Injectable } from '@angular/core';
import { BaseService } from '../base/base-service';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { PreCheck } from 'src/app/shared/models/precheck';
import { PrecheckStateService } from 'src/app/state/precheck-state.service';
import { tap, catchError } from 'rxjs/operators';
import swal from 'sweetalert2'
import { PrePreCheckService } from 'src/app/state/pre-pre-check.service';
@Injectable({
  providedIn: 'root'
})
export class PrecheckService  extends BaseService{

  constructor(
    public http:HttpClient,
    public preCheckSV: PrecheckStateService,
    public prePreCheck: PrePreCheckService
    
  ) { 
    super('/enrollment/pre/precheck')
  }

  getPreCheck():Observable<PreCheck>{
    return this.http.get<PreCheck>(this.fullUrl).pipe(
      tap(x => this.prePreCheck.nextState(x)),
      tap(x => this.preCheckSV.nextState(x)),      
    )
  }

  
}
 