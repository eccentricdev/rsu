import { Injectable } from '@angular/core';
import { BaseService } from '../base/base-service';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { PreCheck } from 'src/app/shared/models/precheck';
import { PrecheckStateService } from 'src/app/state/precheck-state.service';
import { tap, catchError } from 'rxjs/operators';
import swal from 'sweetalert2'
import { LateCheckStateService } from 'src/app/state/late-check-state.service';
import { splitMessage } from '../util/util';
@Injectable({
  providedIn: 'root'
})
export class LatePrecheckService extends BaseService {

  constructor(
    public http:HttpClient,
    public preCheckSV: PrecheckStateService,
    public latePrecheckState :LateCheckStateService
  ) { 
    super('/enrollment/late/precheck')
  }

  getPreCheck():Observable<PreCheck>{
    return this.http.get<PreCheck>(this.fullUrl).pipe(
      tap(x => this.latePrecheckState.nextState(x)),
      tap(x => this.preCheckSV.nextState(x)),      
      catchError(err => {
        console.log(err)
        swal.fire({
          icon: 'error',
          // html: splitMessage(err.error.message),
        })
          return throwError(err)
        }
      )
    )
  }
}
