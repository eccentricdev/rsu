import { Injectable } from '@angular/core';
import { BaseService } from '../base/base-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import swal from 'sweetalert2'
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Subject } from 'src/app/shared/models/subject';
import { AddSubject } from './subject.service';
import { splitMessage } from '../util/util';

@Injectable({
  providedIn: 'root'
})
export class LateSubjectService extends BaseService {

  constructor(
    public http :HttpClient,
  ) {
    super('/enrollment/late/subject')
   }

   searchSubject(subjectCode: String):Observable<Subject>{
    return this.http.get<Subject>(`${this.fullUrl}/${subjectCode}`).pipe(
      catchError(err => {
        swal.fire({
          icon: 'error',
          text: `ไม่พบข้อมูล`,
        })
        return throwError(err)
      })
    )
  }

  addSubject(subject: Subject):Observable<any>{
    return this.http.post(this.fullUrl,subject).pipe(
      catchError(err => {
        swal.fire({
          icon: 'error',
          html: splitMessage(err.error.message),
        })
        return throwError(err)
      })
    )
  }

  deleteSubject(subject: Subject){
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body:subject
    };
    return this.http.delete<AddSubject>(this.fullUrl,httpOptions)
  }
}
