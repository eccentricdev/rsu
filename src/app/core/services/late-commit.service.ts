import { splitMessage } from './../util/util';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base/base-service';
import { Observable, throwError } from 'rxjs';
import { Commit } from 'src/app/shared/models/commit';
import { tap, catchError } from 'rxjs/operators';
import swal from 'sweetalert2'
import { StateCommitService } from 'src/app/state/commit-state.service';
@Injectable({
  providedIn: 'root'
})
export class LateCommitService extends BaseService {

  constructor(
    public http:HttpClient,
    public stateCommit: StateCommitService
  ) { 
   super('/enrollment/late/commit')
  }

  commit():Observable<Commit>{
    return this.http.post<Commit>(this.fullUrl,{}).pipe(
      tap(x => this.stateCommit.nexStateCommit({...x,isCommit: true})),
      tap(x => swal.fire(
        'ยื่นยันรายการเรียบร้อยแล้ว',
        'success'
      )),
      catchError(err => {
        swal.fire({
          icon: 'error',
          html: splitMessage(err.error.message)
        })
        return throwError(err)
        }
      )
    )
  }
}
