import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base/base-service';
import swal from 'sweetalert2'
import { tap, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { StateCommitService } from 'src/app/state/commit-state.service';
import { Commit } from 'src/app/shared/models/commit';
import { splitMessage } from '../util/util';
@Injectable({
  providedIn: 'root'
})
export class LateForceCommitService extends BaseService {

  constructor(
    public http:HttpClient,
    private stateCommit: StateCommitService
  ) {
    super('/enrollment/late/force_commit')
   }

   forceCommit(): Observable<Commit>{
     return this.http.post<Commit>(this.fullUrl,{}).pipe(
      tap(x => this.stateCommit.nexStateCommit({...x,isCommit: true})),
      tap(x => swal.fire(
        'ยื่นยันรายการเรียบร้อยแล้ว',
        'success'
      )),
      catchError(err => {
          swal.fire({
            icon: 'error',
            html: splitMessage(err.error.message),
          })
          return throwError(err)
        }
     )
     )
   }
}
