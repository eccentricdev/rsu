import { Injectable } from '@angular/core';
import { BaseService } from '../base/base-service';
import { Observable, throwError } from 'rxjs';
import { Subject } from 'src/app/shared/models/subject';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import swal from 'sweetalert2'
import { catchError, take } from 'rxjs/operators';
import { splitMessage } from '../util/util';
export interface AddSubject {
  currentCredit: number;
  totalCredit: number;
  creditFee: number;
  academicFee: number;
  accidentInsuranceFee: number;
  fundAmount: number;
  loanAmount: number;
  advanceAmount: number;
  package: number;
  totalPaymentAmount: number;
  academics: Academic[];
  fund?: any;
  loan?: any;
  subjects?: any;
  credits: Credit[];
  debits: any[];
  total: Credit;
  selectedSubjects: SelectedSubject[];
}

export interface SelectedSubject {
  subjectSeq: string;
  educationType: string;
  subjectCode: string;
  subjectNameTh: string;
  subjectNameEn: string;
  credit: number;
  labAmount: number;
  lectureAmount: number;
  isSpecialTopic: string;
  status: string;
  flag: string;
  selectedLectureGroup: SelectedLectureGroup;
  selectedLabGroup?: any;
  selectedTopic?: any;
}

export interface SelectedLectureGroup {
  sectionId: string;
  quota: number;
  confirmSeat: number;
  reserveSeat: number;
  studyDay: string;
  studyStartTime: string;
  studyEndTime: string;
  lectureMidtermDate?: any;
  lectureFinalDate?: string;
  lectureMidtermStartTime?: any;
  lectureMidtermEndTime?: any;
  lectureFinalStartTime?: string;
  lectureFinalEndTime?: string;
  labMidtermDate?: any;
  labFinalDate?: any;
  labMidtermStartTime?: any;
  labMidtermEndTime?: any;
  labFinalStartTime?: any;
  labFinalEndTime?: any;
  lectureMidtermTimeCode?: string;
  lectureFinalTimeCode: string;
  labMidtermTimeCode?: string;
  labFinalTimeCode?: string;
  isFull: boolean;
  studyTime: string;
  topicCode?: any;
}

export interface Credit {
  name: string;
  amount: number;
}

export interface Academic {
  feeCode: string;
  feeName: string;
  amount: number;
}

@Injectable({
  providedIn: 'root'
})
export class SubjectService extends BaseService {

  constructor(
    private http :HttpClient,
  ) { 
    super('/enrollment/pre/subject')
  }

  searchSubject(subjectCode: String):Observable<Subject>{
    return this.http.get<Subject>(`${this.fullUrl}/${subjectCode}`).pipe(
      // tap(x => {}),
      take(1),
      catchError(err => {
        console.log(err)
        swal.fire({
          icon: 'error',
          text: `${err.error.message}`,
        })
        return throwError(err)
      }
      )
    )
  }

  addSubject(subject: Subject):Observable<AddSubject>{
    return this.http.post<AddSubject>(this.fullUrl,subject).pipe(
      catchError(err => {
        swal.fire({
          icon: 'error',
          html: splitMessage(err.error.message),
        })
        return throwError(err)
      })
    )
  }

  deleteSubject(subject: Subject){
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body:subject
  };
    return this.http.delete<AddSubject>(this.fullUrl,httpOptions).pipe(
      catchError(err => {
        console.log(err)
        swal.fire({
          icon: 'error',
          text: "Look like your account is not correct, Please try again",
        })
        return throwError(err)
      })
    )
  }
}
