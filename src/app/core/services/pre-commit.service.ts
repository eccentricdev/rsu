import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from '../base/base-service';
import { Observable, throwError } from 'rxjs';
import { Commit } from 'src/app/shared/models/commit';
import { tap, catchError } from 'rxjs/operators';
import swal from 'sweetalert2'
import { StateCommitService } from 'src/app/state/commit-state.service';
import { PreForceCommitService } from './pre-force-commit.service';
import { splitMessage } from '../util/util';

@Injectable({
  providedIn: 'root'
})
export class PreCommitService extends BaseService {

  constructor(
    public http:HttpClient,
    public stateCommit: StateCommitService,
    public preForceCommitSV :PreForceCommitService
  ) { 
    super('/enrollment/pre/commit')
  }

  commit():Observable<Commit>{
    return this.http.post<Commit>(this.fullUrl,{}).pipe(
      tap(x => this.stateCommit.nexStateCommit({...x,isCommit: true})),
      tap(x => swal.fire({
        text:'บันทึกรายวิชาเรียบร้อยแล้ว',
        confirmButtonText:'ตกลง'
      })),
      catchError(err => {
          swal.fire({
            icon: 'error',
            html: splitMessage(err.error.message),
          })
          return throwError(err)
        }
      )
    )
  }
}
