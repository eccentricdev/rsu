import { Payment } from './../../../shared/models/payment';
import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import swal from 'sweetalert2'
import { StateCommitService } from 'src/app/state/commit-state.service';
@Injectable({
  providedIn: 'root'
})
export class AlipayService extends BaseService {

  constructor(
    public http:HttpClient,
    public commitState: StateCommitService
  ) {
    super('/payment/alipay')
   }

   alipay():Observable<Payment>{
    return this.http.get<Payment>(`${this.fullUrl}/${this.commitState.getvalue().payInNo}`).pipe(
      tap(x => console.log(x)),
      tap(x => swal.fire(
        'ยื่นยันรายการเรียบร้อยแล้ว',
        'success'
      ))
    )
   }
}
