import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface paymentPrecheck {
  enrollmentSummary: EnrollmentSummary;
  isAllowPayment: boolean;
  lastPayment?: any;
  waitingSubjects: any[];
  waitingList: any[];
  payments: Payment[];
  paymentMethods: PaymentMethod[];
}

export interface PaymentMethod {
  payment_method_code: string;
  payment_method_name: string;
  logo_url: string;
  fee: number;
  amount: number;
  total_amount: number;
  row_order: number;
  api_url: string;
  message?: string
}

export interface Payment {
  registerDate: string;
  txRunning?: any;
  registerType: string;
  totalCredit: number;
  totalSubject: number;
  totalAmount: number;
  payInNo: string;
  payInDate?: any;
}

export interface EnrollmentSummary {
  totalCredit: number;
  creditFee: number;
  academicFee: number;
  accidentInsuranceFee: number;
  fundAmount: number;
  loanAmount: number;
  advanceAmount: number;
  package: number;
  totalPaymentAmount: number;
  payInNo: string;
  academics?: any;
  fund?: any;
  loan?: any;
  subjects: Subject[];
  credits?: any;
  debits?: any;
  total?: any;
  payEnable: boolean;
  type: string
}

export interface Subject {
  subjectCode: string;
  subjectName: string;
  credit: number;
  section: string;
  lectureAmount: number;
  labAmount: number;
  totalAmount?: any;
  status?: any;
}
@Injectable({
  providedIn: 'root'
})
export class PaymentPrecheckService   extends BaseService{

  constructor(
    public http:HttpClient
  ) {
    super('/payment/precheck')
   }

    getPreCheckPayment():Observable<paymentPrecheck>{
      return this.http.get<paymentPrecheck>(this.fullUrl).pipe(
        
      )
    }
} 
