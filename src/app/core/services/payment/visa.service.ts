import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { HttpClient } from '@angular/common/http';
import { StateCommitService } from 'src/app/state/commit-state.service';
import { tap } from 'rxjs/operators';
import swal from 'sweetalert2'
import { Observable } from 'rxjs';
import { Payment } from 'src/app/shared/models/payment';
@Injectable({
  providedIn: 'root'
})
export class VisaService extends BaseService {

  constructor(
    public http: HttpClient,
    public commitState: StateCommitService
  ) { 
    super('/payment/creditcard/web')
  }

  creditCard(payInNo: string): Observable<Payment>{
    // console.log(this.commitState.getvalue().payInNo)
    return this.http.get(`${this.fullUrl}/${payInNo}`).pipe(
      tap(x => console.log(x)),
      tap(x => swal.fire(
        'ยื่นยันรายการเรียบร้อยแล้ว',
        'success'
      ))
    )
  }
}
