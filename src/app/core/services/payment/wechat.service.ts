import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Payment } from 'src/app/shared/models/payment';
import { tap } from 'rxjs/operators';
import swal from 'sweetalert2'
import { StateCommitService } from 'src/app/state/commit-state.service';
@Injectable({
  providedIn: 'root'
})
export class WechatService extends BaseService {

  constructor(
    public http:HttpClient,
    public commitState: StateCommitService
  ) { 
    super('/payment/wechat')
  }

  wechatPayment(): Observable<Payment>{
    return this.http.get(`${this.fullUrl}/${this.commitState.getvalue().payInNo}`).pipe(
      tap(x => console.log(x)),
      tap(x => swal.fire(
        'ยื่นยันรายการเรียบร้อยแล้ว',
        'success'
      ))
    )
  }
}
