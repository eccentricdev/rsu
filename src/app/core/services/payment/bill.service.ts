import { Injectable } from '@angular/core';
import { BaseService } from '../../base/base-service';
import { HttpClient } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import swal from 'sweetalert2'
import { splitMessage } from '../../util/util';

@Injectable({
  providedIn: 'root'
})
export class BillService extends BaseService {

  constructor(
    public http:HttpClient
  ) {
    super('/payment/bill')
   }

   billing(number:string = '622904373'){     
     return this.http.get(`${this.fullUrl}/${number}`).pipe(
        tap(x => console.log(x)),
        catchError(err => {
          swal.fire({
            icon: 'error',
            html: splitMessage(err.error.message),
          })
          return  throwError(err)
        })
     )
   }
}
