import { FormGroup, FormBuilder } from '@angular/forms';

export class Baseform {
    form:FormGroup
    formBuilder:FormBuilder
    constructor(formBuilder:FormBuilder){
        this.formBuilder = formBuilder
        this.form = this.crateForm()
    }


    crateForm(){
        return this.formBuilder.group({})
    }
}