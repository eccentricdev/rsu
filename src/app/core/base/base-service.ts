
export class BaseService {
    // protected prefix:string = 'http://110.164.184.31:5800'
    
    //use here
    protected prefix:string = 'https://pg.rsu.ac.th:5801'
    
    // protected prefix:string = 'http://api.giftver.com'
    protected fullUrl:string = ''
    
    constructor(protected endpoint:string) {
        this.fullUrl = this.prefix + this.endpoint
    }
}