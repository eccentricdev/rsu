import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { tap, take, catchError } from 'rxjs/operators';
import { StateAppService } from 'src/app/state/state-app.service';
import { Router } from '@angular/router';

@Injectable()
export class HTTPInterceptor implements HttpInterceptor {
  token: string = ''
  constructor(
    public appStateSV : StateAppService,
    private router:Router
  ) {

    this.appStateSV.getStateChange().pipe(      
      tap(x => this.token = x.token)
    ).subscribe() 

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const customReq = request.clone({
        setHeaders:{
            Authorization:`Bearer `+ this.token
        }
    });
    return next.handle(customReq).pipe(
        catchError(err => {
          if (err.error.code == '-1') {
            this.router.navigate(['/login'], {replaceUrl: true})
          } 

          return throwError(err)            
        })
    );
  }
}