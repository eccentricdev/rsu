import { ProfileStudentStateService } from 'src/app/state/profile-student-state.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, CanActivateChild, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { StateAppService } from 'src/app/state/state-app.service';
import swal from 'sweetalert2'
import { PrecheckStateService } from 'src/app/state/precheck-state.service';
import { tap } from 'rxjs/operators';
import { PrecheckService } from '../services/precheck.service';
import { PrePreCheckService } from 'src/app/state/pre-pre-check.service';
import { splitMessage } from '../util/util';

@Injectable({
  providedIn: 'root'
})

export class AuthGuardGuard implements CanActivate,CanActivateChild {
  constructor(
    private statapp: StateAppService,
    private route:Router,
    public preCheckSV:PrecheckService,
    public prePreCheck: PrePreCheckService
  ) {    

  }

  canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if(this.statapp.getValue().isLogin){
      console.log('log from late precheck Guard',this.prePreCheck.getValue())
      if (this.prePreCheck.getValue().isEligibleToEnroll == false && this.prePreCheck.getValue().isReadOnly == false) {
          swal.fire({
            icon: 'error',
            // text: this.prePreCheck.getValue().message,
            html: splitMessage(this.prePreCheck.getValue().message)
          })
          return false
      }

      return true
      
    } else {
      swal.fire({
        icon: 'error',        
        text: "Please Login First",
      })
      this.route.navigate(['/login'],{replaceUrl: true})
    }
  }

  canActivateChild(route:ActivatedRouteSnapshot,state:RouterStateSnapshot):Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{
    return this.canActivate(route,state)
  }
  
}
