import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate } from '@angular/router';
import swal from 'sweetalert2'
import { Observable } from 'rxjs';
import { ProfileStudentStateService } from 'src/app/state/profile-student-state.service';
export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
  providedIn: 'root'
})

export class SelectStudentGuard implements CanDeactivate<CanComponentDeactivate> {
  constructor(private profileStudentState: ProfileStudentStateService) {    
  }

  canDeactivate(
      component: CanComponentDeactivate, 
      currentRoute: ActivatedRouteSnapshot, 
      currentState: RouterStateSnapshot, 
      nextState?: RouterStateSnapshot
      ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree>
  {
    let nextUrl = nextState.url 
    if(nextUrl == '/rsu/home') {
      return true
    } {
      let statStudent = this.profileStudentState.getStudentProfile()    
      return statStudent ? true : this.noti() 
    }
    
  }

  
  private noti(){
    swal.fire({
      icon: 'error',
      text: "Please select student first",
    })
    return false
  }
  
}
