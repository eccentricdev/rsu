import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { StateAppService } from 'src/app/state/state-app.service';
import { LateCheckStateService } from 'src/app/state/late-check-state.service';
import swal from 'sweetalert2'
import { splitMessage } from '../util/util';

@Injectable({
  providedIn: 'root'
})
export class LatePrecheckGuard implements CanActivate {
  
  constructor(
    private statapp: StateAppService,
    private latePreCheck: LateCheckStateService,
    private route:Router
  ){

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      console.log('log from late precheck Guard',this.latePreCheck.getValue())
      if(this.statapp.getValue().isLogin){
        if (this.latePreCheck.getValue().isEligibleToEnroll == false && this.latePreCheck.getValue().isReadOnly == false) {
          swal.fire({
            icon: 'error',
            html: splitMessage(this.latePreCheck.getValue().message)            
            // text: this.latePreCheck.getValue().message,
          })
          return false
      }
  
        return true                
      } else {
        swal.fire({
          icon: 'error',
          text: "Please Login First",
        })
        this.route.navigate(['/login'],{replaceUrl: true})
      }
  }
  
}
