import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeContainer } from './home/home/home.container';
import { HomeComponent } from './home/home/home/home.component';
import { HomeModule } from './home/home.module';
import { AuthGuardGuard } from './core/guards/auth-guard.guard';
import { SelectStudentGuard } from './core/guards/select-student.guard';
import { LatePrecheckGuard } from './core/guards/late-precheck.guard';
import { UnsaveGuard } from './core/guards/unsave.guard';

const routes: Routes = [
  {
    path:'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginModule)
  },
  {
    path:'slip',
    canActivate:[AuthGuardGuard],
    loadChildren: () => import('./slip/slip.module').then(x => x.SlipModule)
  },
  // {
  //   path:'optionslip',
  //   canActivate:[AuthGuardGuard],
  //   loadChildren: () => import('./slipwithoption/slipwithoption.module').then(x => x.SlipwithoptionModule)
  // },
  {
    path:'',
    redirectTo:'rsu/home',
    pathMatch:'full'
  },
  {
    path:'rsu',
    component:HomeContainer,
    children:[      
      {
        path:'home',
        component:HomeComponent,
        data:{
          topic:'ข่าวประชาสัมพันธ์',
          sub_topic:'ยินดีต้อนรับ'
        },
      },
      {
        path:'optionslip',
        canActivate:[AuthGuardGuard],
        loadChildren: () => import('./slipwithoption/slipwithoption.module').then(x => x.SlipwithoptionModule),
        data : {
          topic:'ชำระเงิน',
          sub_topic:'ชำระเงิน',
        },        
      },
      {
        path:'register',
        canActivate:[AuthGuardGuard],
        loadChildren: () => import('./register-subject/register-subject.module').then(r => r.RegisterSubjectModule),
        data : {
          topic:'งานทะเบียน',
          sub_topic:'ลงทะเบียนล่วงหน้า',
          state:'pre'
        },        
      },
      {
        path:'managesubject',
        canActivate:[AuthGuardGuard],
        loadChildren: () => import('./manage-subject/manage-subject.module').then(r => r.ManageSubjectModule),
        data : {
          topic:'งานทะเบียน',
          sub_topic:'เพิ่ม ถอนวิชา',
          state:'add-remove'
        }
      },
      {
        path:'late',
        canActivate:[LatePrecheckGuard],
        loadChildren: () => import('./register-subject/register-subject.module').then(r => r.RegisterSubjectModule),
        data : {
          topic:'งานทะเบียน',
          sub_topic:'ลงทะเบียนล่าช้า',
          state:'late'
        }
      },
      {
        path:'select-student',
        canActivate:[AuthGuardGuard],
        canDeactivate:[SelectStudentGuard],
        loadChildren: () => import('./login-as-student/login-as-student.module').then(r => r.LoginAsStudentModule),
        data : {
          topic:'งานทะเบียน',
          sub_topic:'เลือกนักเรียน',
          state:'select-student'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    HomeModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
