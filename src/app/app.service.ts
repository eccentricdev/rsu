import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class AppService {
  private openPopUp$ = new BehaviorSubject<string>(null)
  private _openPopUp$ = this.openPopUp$.asObservable().pipe(filter(x => x != null))
  constructor() { }

  onDialogOpenChange(): Observable<string>{
    return this._openPopUp$
  }

  onClick(){
    this.openPopUp$.next('')
  }
}
