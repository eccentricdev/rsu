import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RegisterSubjectComponent } from './container/register-subject/register-subject.component';
import { TopicSubjectComponent } from './presentational/topic-subject/topic-subject.component';
import { ListSubjectComponent } from './presentational/list-subject/list-subject.component';
import { TuituinFeeComponent } from './presentational/tuituin-fee/tuituin-fee.component';
import { SharedModule } from '../shared/shared.module';

const routes: Routes  = [
  {
    path:'',
    component:RegisterSubjectComponent
  }
]

@NgModule({
  declarations: [
    RegisterSubjectComponent,
    TopicSubjectComponent,
    ListSubjectComponent,
    TuituinFeeComponent,
  ],
  
  imports: [
    SharedModule,
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ]
})
export class RegisterSubjectModule { }
