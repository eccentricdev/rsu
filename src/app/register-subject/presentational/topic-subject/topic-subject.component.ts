import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { Subject, AvailableLectureGroup } from 'src/app/shared/models/subject';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-topic-subject',
  templateUrl: './topic-subject.component.html',
  styleUrls: ['./topic-subject.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class TopicSubjectComponent implements OnInit,OnChanges {
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    console.log(changes)
  }
  @Input() subject:Subject[] =  []
  @Input() isReadOnly: boolean
  @Input() form:FormGroup
  @Input() totalCredit: number = 0
  @Input() searchResult$:Observable<Subject>
  @Input() totalPayamount: number = 0
  @Output() onSearchSubject = new EventEmitter<string>()
  @Output() onSelectSubject = new EventEmitter<AvailableLectureGroup>()
  @ViewChild('search') private inputSearchSubject:ElementRef<HTMLInputElement>

  constructor() { }

  ngOnInit(): void {
  }

  searchSubject(){
    let subjectCode:string = this.inputSearchSubject.nativeElement.value.toUpperCase()
    this.onSearchSubject.emit(subjectCode)
  }

  upperCase(){    
    this.inputSearchSubject.nativeElement.value = this.inputSearchSubject.nativeElement.value.toUpperCase()
  }

  selectSubject(subject: AvailableLectureGroup){
    this.searchResult$ = null    
    this.onSelectSubject.emit(subject)
  }
}
