import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'src/app/shared/models/subject';
import { StudentProfile } from 'src/app/shared/models/profile-student';
import { PreCheck } from 'src/app/shared/models/precheck';


@Component({
  selector: 'app-list-subject',
  templateUrl: './list-subject.component.html',
  styleUrls: ['./list-subject.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListSubjectComponent implements OnInit {
  @Input() form:FormGroup
  @Input() isReadOnly: boolean
  @Input() totalAmount: number = 0
  @Input() profileStudent: StudentProfile
  @Input() isCommitSuccess = false;
  @Input() data: PreCheck
  @Output() onDeleteSubject = new EventEmitter<Subject>()
  @Output() onSave = new EventEmitter<void>()
  @Output() onCommit = new EventEmitter<void>()

  
  
  constructor() { 

  }

  ngOnInit(): void {
    
  }

  deleteSubject(subject:Subject){
    this.onDeleteSubject.emit(subject)
  }

  onClickSave(){
    this.onSave.emit()
  }
  
  onClickCommit(){    
    this.onCommit.emit()
  }


  
}
