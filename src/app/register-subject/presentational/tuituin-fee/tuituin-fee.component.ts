import { Component, OnInit, ChangeDetectionStrategy, Input, OnChanges, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { Commit } from 'src/app/shared/models/commit';
import { Router } from '@angular/router';
import { PreCheck } from 'src/app/shared/models/precheck';


@Component({
  selector: 'app-tuituin-fee',
  templateUrl: './tuituin-fee.component.html',
  styleUrls: ['./tuituin-fee.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class TuituinFeeComponent implements OnInit,OnChanges {
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    console.log(this.realCommit)
    console.log(this.commit)
    // if (this.realCommit.isCommit) {
    //   this.realCommit.credits.forEach(x => this.sumAmount += x.amount)
    //   this.realCommit.debits.forEach(x => this.sumDebit += x.amount )  
    // }
  }
  @Input() commit:Commit
  @Input() totalPayamount: number = 0
  @Input() realCommit: Commit
  @Input() isCommitSuccess: boolean = false
  @Input() precheck: PreCheck
  @Output() onCommitEvent = new EventEmitter<void>()
  sumAmount: number = 0
  sumDebit: number = 0

  constructor(
    public router:Router,
    // private cdRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    if (this.commit.credits && this.commit.debits) {
      if (this.commit.credits.length != 0) {
        this.commit.credits.forEach(x => this.sumAmount += x.amount)
        
      }
  
      if (this.commit.debits.length != 0 ) {
        this.commit.debits.forEach(x => this.sumDebit += x.amount )  
      } 
    }    
  }

  openPopUp(){
    let element = document.getElementById('modal')
    element.style.display = 'block'
  }

  gotoSlip(){
    this.router.navigate(['/slip'])
  }

  gotoSlipWithOption(){
    this.router.navigate(['rsu/optionslip'])
  }

  onCommit(){
    this.onCommitEvent.emit()
  }

}
