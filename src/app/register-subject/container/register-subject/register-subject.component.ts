
import { Subject as SubjectModal }  from './../../../shared/models/subject';
import { Component, OnInit, ChangeDetectorRef, OnDestroy, ViewChild, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { SubjectService } from 'src/app/core/services/subject.service';
import { tap, catchError, takeUntil, map, switchMap, delay, finalize, debounceTime, take, skip } from 'rxjs/operators';
import { Baseform } from 'src/app/core/base/base-form';
import { Observable, of, Subject, throwError } from 'rxjs';
import { PreCheck, SelectedSubject } from 'src/app/shared/models/precheck';
import { PrecheckService } from 'src/app/core/services/precheck.service';
import { FormBuilder, FormArray } from '@angular/forms';
import swal from 'sweetalert2'
import { ActivatedRoute, Router } from '@angular/router';
import { LatePrecheckService } from 'src/app/core/services/late-precheck.service';
import { LateSubjectService } from 'src/app/core/services/late-subject.service';
import { PreCommitService } from 'src/app/core/services/pre-commit.service';
import { LateCommitService } from 'src/app/core/services/late-commit.service';
import { StateCommitService } from 'src/app/state/commit-state.service';
import { Commit } from 'src/app/shared/models/commit';
import { LateForceCommitService } from 'src/app/core/services/late-force-commit.service';
import { PreForceCommitService } from 'src/app/core/services/pre-force-commit.service';
import { ProfileStudentStateService } from 'src/app/state/profile-student-state.service';
import { StudentProfile } from 'src/app/shared/models/profile-student';
import { paymentPrecheck, PaymentPrecheckService } from 'src/app/core/services/payment/payment-precheck.service';
import { splitMessage } from 'src/app/core/util/util';
import { PrecheckStateService } from 'src/app/state/precheck-state.service';
import { __exportStar } from 'tslib';

@Component({
  selector: 'app-register-subject',
  templateUrl: './register-subject.component.html',
  styleUrls: ['./register-subject.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class RegisterSubjectComponent extends  Baseform implements OnInit,OnDestroy {
  @ViewChild('tuituin') resultCommit:ElementRef<HTMLDivElement>
  precheck$: Observable<PreCheck>
  searchResult$: Observable<SubjectModal>
  paymentPreCheck$ = new Observable<paymentPrecheck>()
  totalAmount: number = 0
  totalCredit: number = 0  
  state:string = ''
  stateCommit$: Observable<Commit>
  searchResult:SubjectModal
  private handleErrorSubject$ = new Subject()
  private onDestroy$ = new Subject()
  profileStudent :StudentProfile
  currentCredit: number  = 0
  isCommitSuccess = false
  sumAmount: number = 0
  sumDebit: number = 0
  displayCommit
  totalPayamount: number = 0
  totaldebit: number  = 0

  data: PreCheck
  isStart: boolean = true


  constructor(
    public subjectSV: SubjectService,
    public lateSubjectSV:LateSubjectService,
    public precheck:PrecheckService,
    public latePrecheck: LatePrecheckService,
    public fb:FormBuilder,
    public cdRef:ChangeDetectorRef,
    public activeRoute: ActivatedRoute,
    public preCommitSV: PreCommitService,
    public lateCommitSV: LateCommitService,
    public stateCommitSV :StateCommitService,
    public preForceCommitSV :PreForceCommitService,
    public lateForceCommitSV :LateForceCommitService,
    public profileState: ProfileStudentStateService,
    public PaymentPrecheckSV: PaymentPrecheckService,
    public router:Router,
    
  ) { 
    super(fb)
    this.profileStudent = this.profileState.getStudentProfile()
  }

  ngOnInit(): void {
      this.activeRoute.data.pipe(
        tap(x => this.state = x.state),
      ).subscribe() 

      this.stateCommit$ = this.stateCommitSV.getStateChange()
      // .pipe(
      //   tap(x => this.paymentPreCheck$ = this.PaymentPrecheckSV.getPreCheckPayment())
      // )
      this.getPreheck()
      this.HandleErrornWaiting()
  }

  sumTotalPayment(){
    if (this.displayCommit.credits) {
      this.displayCommit.credits.forEach(x => this.totalPayamount += x.amount)  
      console.log(this.totalPayamount)
    }

    if (this.displayCommit.debits) {
      this.displayCommit.debits.forEach(x => this.totalCredit += x.amount)  
      console.log(this.totalCredit)
    }
    
  }

  private getPreheck(){    
    if(this.state == 'pre'){
        this.precheck$ = this.precheck.getPreCheck().pipe(          
          tap(x => console.log(x)),
          tap(x => this.addform(x)),
          // tap(x => this.currentCredit = x.currentCredit),
          tap(x => {
            if (this.isStart) {
                this.displayCommit = x
                this.currentCredit = x.currentCredit
                this.sumTotalPayment()
                // this.totalPayamount = x.totalPaymentAmount
            }
          }),
          tap(x => this.data = x),
          catchError(err => {
            console.log(err)
            // this.router.navigate(['/rsu/home'])
            swal.fire({
              icon: 'error',
              html: splitMessage(err.error.message),
            })
            return  throwError(err)
          })
        )
    } else {
        this.precheck$ = this.latePrecheck.getPreCheck().pipe(
          tap(x => console.log(x)),
          tap(x => this.addform(x)),
          // tap(x => this.currentCredit = x.currentCredit),
          tap(x => {
            if (this.isStart) {
                this.displayCommit = x
                this.currentCredit = x.currentCredit
                this.sumTotalPayment()
                // this.totalPayamount = x.totalPaymentAmount
            }
          }),
          tap(x => this.data = x),
          catchError(err => {
            swal.fire({
              icon: 'error',
              html: splitMessage(err.error.message),
            })
            return  throwError(err)
          })
        )
    }     
  }

  private HandleErrornWaiting(){
    this.handleErrorSubject$.pipe(
      tap(x => console.log('Log from handle error')),      
      tap(x => console.log('event from popup',x)),
      switchMap(x => {
        if (this.state == 'pre') {
            return this.preForceCommitSV.forceCommit().pipe(
              tap(x => console.log(x)),
              tap(x => this.clearForm()),
              tap(x => this.getPreheck()),
              tap(x => this.scrollToComponent()),
              tap(x => this.isCommitSuccess = true)
            )
        }
        
        return this.lateForceCommitSV.forceCommit().pipe(
              tap(x => console.log(x)),
              tap(x => this.clearForm()),
              tap(x => this.getPreheck()),
              tap(x => this.scrollToComponent()),           
              tap(x => this.isCommitSuccess = true)
        )
      }),
      takeUntil(this.onDestroy$)
    ).subscribe()
  }

  private alertCheckCanEnroll(canEnroll: boolean){
    if(!canEnroll){
      swal.fire({
        icon: 'error',
        text: "You can not Enroll now!",
      })
    }
  }


  searchSubject(subjectCode:string){ 
    console.log('call api search')   
    if (this.state == 'pre') {
        this.searchResult$ = this.subjectSV.searchSubject(`${subjectCode}`).pipe(
          tap(_ => console.log('call')),
          tap(x => this.searchResult = x),
          tap(x => this.openPopUp())
        )
    } else {
      this.searchResult$ = this.lateSubjectSV.searchSubject(`${subjectCode}`).pipe(
        tap(x => this.searchResult = x),
        tap(x => this.openPopUp())
      )  
    }    
  }


  selectSubject(subject: SubjectModal){    
    this.openPopUp() 
  }

  private checkMaxSubject(newResult): boolean{
    return this.form.get('maxCredit').value < (this.currentCredit + newResult.credit)
  }

  // todo check min

  selectSubjectFromPopUp(request){
    console.log(this.searchResult)
    console.log(request)
    let newResult = {
      ...this.searchResult,
      credit: this.searchResult.isSpecialTopic == "Y" ? request.selectedTopic.topicCredit : this.searchResult.credit,
      ...request
    }

    if (this.checkMaxSubject(newResult)) {
      swal.fire({
        icon: 'error',
        // html: splitMessage(err.error.message),
        text:'จำนวนหน่วยกิตที่ลงทะเบียนมากกว่าที่กำหนดไว้'
      })
      return
    }
    delete newResult.availableLectureGroups    
    delete newResult.availableTopics
    delete newResult.availableLabGroups
    this.defaultValue()
    if (this.state == 'pre') {
      this.subjectSV.addSubject(newResult).pipe(
        tap(x => console.log(x)),
        tap(x => this.clearForm()),
        tap(x => this.isStart  = false),
        // tap(x => this.searchResult$ = null),
        tap(x => this.closePopUp()),
        tap(x => this.getPreheck()),
        tap(x => swal.fire({
          text:'บันทึกรายวิชาเรียบร้อยแล้ว',
          confirmButtonText:'ตกลง'
        },          
        )),
        tap(x => this.currentCredit  = x.currentCredit),
        // tap(x => this.totalPayamount = x.totalPaymentAmount),
        tap(() => this.sumTotalPayment()),
        tap(x => this.displayCommit = x)
      ).subscribe() 
    } else {
      this.lateSubjectSV.addSubject(newResult).pipe(
        tap(x => console.log(x)),
        tap(x => this.clearForm()),
        tap(x => this.isStart  = false),
        // tap(x => this.searchResult$ = null),
        tap(x => this.closePopUp()),
        tap(x => this.getPreheck()),
        tap(x => swal.fire(
          'บันทึกรายวิชาเรียบร้อยแล้ว'
        )),
        tap(x => this.currentCredit = x.currentCredit),
        tap(x => this.totalPayamount = x.totalPaymentAmount),
        tap(x => this.displayCommit = x)
      ).subscribe()
    }
  }


  deleteSubject(subject: SubjectModal){    

    swal.fire({
      title: `ต้องการที่จะลบวิชา ${subject.subjectCode} หรือไม่ ?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'ตกลง',
      cancelButtonText:'ยกเลิก'      
    }).then((result) => {
      if (result.value) {
        this._deleteSubject(subject)
      }
    })
  }


  private _deleteSubject(subject: SubjectModal){
    this.defaultValue()
    if (this.state == 'pre') {
      this.subjectSV.deleteSubject(subject).pipe(
        tap(x => this.isStart  = false),
        tap(x => console.log(x)),
        tap(x => this.clearForm()),
        tap(x => this.searchResult$ = null),
        tap(x => this.getPreheck()),        
        tap(x => this.displayCommit = x),
        tap(x => this.currentCredit = x.currentCredit),
        tap(x => this.totalPayamount = x.totalPaymentAmount)
      ).subscribe()      
    } else {
      this.lateSubjectSV.deleteSubject(subject).pipe(
        tap(x => this.isStart  = false),
        tap(x => console.log(x)),
        tap(x => this.clearForm()),
        tap(x => this.searchResult$ = null),
        tap(x => this.getPreheck()),
        tap(x => this.displayCommit = x),
        tap(x => this.currentCredit = x.currentCredit),
        tap(x => this.totalPayamount = x.totalPaymentAmount)
      ).subscribe()
    }      
  }



  defaultValue(){
    this.totalAmount = 0
    this.totalCredit = 0
  }

  addform(item:PreCheck){
    this.form.patchValue({
      minCredit: item.minCredit,
      maxCredit: item.maxCredit,
      academicYear: item.academicYear,
      academicSemester: item.academicSemester,
      isEligibleToEnroll: item.isEligibleToEnroll,
      message: item.message
    })

    let form = this.form.get('selectedSubjects') as FormArray;
    item.selectedSubjects.map( subject => {
      form.push(this.addForm(subject))
      this.totalAmount += subject.lectureAmount
      // this.totalCredit += subject.credit
    })
  }

  commit(event){
    swal.fire({
      title: 'หากยืนยันการลงทะเบียนแล้ว หมายถึงพร้อมชำระเงิน ไม่สามารถแก้ไขการการลงทะเบียนได้',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'เสร็จสิ้นการลงทะเบียน',
      cancelButtonText:'ยกเลิก'      
    }).then((result) => {
      if (result.value) {
        this._commit()
      }
    })
  }

  _commit(){
    if (this.state == 'pre') {
        this.preCommitSV.commit().pipe(
          tap(x => console.log(x)),
          tap(x => this.clearForm()),
          tap(x => this.getPreheck()),
          tap(x => this.isCommitSuccess = true),
          tap(x => this.scrollToComponent()),
          // tap(x => this.paymentPreCheck$ = this.PaymentPrecheckSV.getPreCheckPayment()),
          catchError(err => {
            if (err.error &&  (err.error.code == "HAVE_WAITING_LIST")) {
                  swal.fire({
                    title: 'ต้องการที่จะดำเนินต่อไหม',
                    text: err.error.message,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',                    
                    confirmButtonText: 'ยืนยัน',
                    cancelButtonText:'ยกเลิก'                  
                  }).then((result) => {
                      if (result.value) {
                        this.handleErrorSubject$.next(true)
                      }                  
                  })
            }
            return of([])
          })
        ).subscribe()
    } else {
        this.lateCommitSV.commit().pipe(
          tap(x => this.clearForm()),
          tap(x => this.getPreheck()),
          tap(x => this.isCommitSuccess = true),
          tap(x => this.scrollToComponent()),
        ).subscribe()
    }
  }

  
  
  crateForm(){
    return this.formBuilder.group({
      minCredit:[],
      maxCredit:[],
      academicYear:[],
      academicSemester:[],
      isEligibleToEnroll:[],
      message:[],
      flag:[],
      selectedSubjects:this.formBuilder.array([

      ])
    })
  }

public scrollToComponent(){  
  setTimeout(() => {
    if (this.resultCommit) {
      this.resultCommit.nativeElement.scrollIntoView({
        behavior: "smooth"
      }); 
    }  
  }, 500);  
}

  private clearForm(){
    let form = this.form.get('selectedSubjects') as FormArray
    form.clear()
  }

  private openPopUp(){
    let element = document.getElementById('dynamicmodal')
    element.style.display = 'block'
  }

  private closePopUp(){
    let element = document.getElementById('dynamicmodal')
    element.style.display = 'none'
  }

  private addForm(item:SelectedSubject){
    return this.formBuilder.group({
      subjectSequence:[item.subjectSequence],
      subjectCode:[item.subjectCode],
      subjectNameTh:[item.subjectNameTh],
      subjectNameEn:[item.subjectNameEn],
      credit:[item.credit],
      labAmount:[item.labAmount],
      lectureAmount:[item.lectureAmount],
      isSpecialTopic:[item.isSpecialTopic],
      status:[item.status],
      selectedLectureGroup:[item.selectedLectureGroup],
      selectedLabGroup:[item.selectedLabGroup],
      selectedTopic:[item.selectedTopic],
      flag:[item.flag]
    })
  }


  ngOnDestroy(): void {
      this.onDestroy$.next(true)
      this.onDestroy$.complete()
  } 

}
