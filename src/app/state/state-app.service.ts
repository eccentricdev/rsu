import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ResultLogin } from '../shared/models/resul-login';

export interface StateApp {
  name:string,
  isLogin:boolean,
  asName:string,
  token:string,
  isPayment:boolean,
  menus: Menu[];
  roles: string[];
  
}
export interface Menu {
  code: string;
  name: string;
  visible: boolean;
  enable: boolean;
}

@Injectable({
  providedIn: 'root'
})

export class StateAppService {
  private initState: StateApp = {name:'',isLogin:false,isPayment:false,token:'',asName:'',menus:[],roles:[]}
  private stateApp$ = new BehaviorSubject<StateApp>(this.initState)
  constructor() { }

  getValue():StateApp {
    return this.stateApp$.getValue()
  }

  getStateChange(): Observable<StateApp>{
    return this.stateApp$
  }

  isStudent():boolean {
    return this.stateApp$.getValue().roles.includes('STUDENT')
  }

  stateChangeLogin(result:ResultLogin){
    this.stateApp$.next({
        ...this.initState,
        name:result.name,
        asName:result.asName,
        isLogin:true,
        isPayment:false,
        token:result.token,
        menus:[...result.menus],
        roles:[...result.roles]
        
    })
  }

  stateChangeLogout(){
    this.stateApp$.next({
      ...this.initState,
      name:'',
      isLogin:false,
      isPayment:false,
      token: '',
      menus:[...[]],
      roles:[...[]]
    })
  }
}
