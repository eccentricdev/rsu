import { Injectable } from '@angular/core';
import { Commit } from '../shared/models/commit';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { tap, map, switchMapTo } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class StateCommitService {
  private initState:Commit = {
    
    isCommit: false,
    totalCredit: 0,
    creditFee: 0,
    academicFee: 0,
    accidentInsuranceFee: 0,
    fundAmount: 0,
    loanAmount: 0,
    advanceAmount: 0,
    package: 0,
    totalPaymentAmount: 0,
    academics: [],
    fund: null,
    loan: null,
    payInNo:'',
    subjects: [],
    credits: [],
    debits: [],
    payEnable: false,
    total: null
  }

  mockdata:any = {
    isCommit: true,
    "totalCredit": 9,
    "creditFee": 15200,
    "academicFee": 10000,
    "accidentInsuranceFee": 0,
    "fundAmount": 0,
    "loanAmount": 25200,
    "advanceAmount": 0,
    "package": 0,
    "totalPaymentAmount": 0,
    "payInNo": "622900026",
    "academics": [
      {
        "feeCode": "001",
        "feeName": "ค่าบำรุงการศึกษา",
        "amount": 10000
      }
    ],
    "fund": null,
    "loan": {
      "contractNo": "62-0948",
      "amount": 125800
    },
    "subjects": [
      {
        "subjectCode": "ENL128",
        "subjectName": "การนำเสนอผลงานเป็นภาษาอังกฤษ",
        "credit": 3,
        "section": "02",
        "lectureAmount": 4200,
        "labAmount": 0,
        "totalAmount": 4200,
        "status": null
      },
      {
        "subjectCode": "ENG246",
        "subjectName": "การฟัง-การพูด 1",
        "credit": 3,
        "section": "10",
        "lectureAmount": 5100,
        "labAmount": 200,
        "totalAmount": 5300,
        "status": null
      },
      {
        "subjectCode": "HOS112",
        "subjectName": "ความรู้เกี่ยวกับอาหารและเครื่องดื่ม",
        "credit": 3,
        "section": "01",
        "lectureAmount": 5700,
        "labAmount": 0,
        "totalAmount": 5700,
        "status": null
      }
    ],
    "credits": [
      {
        "name": "ค่าหน่วยกิต",
        "amount": 15200
      },
      {
        "name": "ค่าบำรุงการศึกษา",
        "amount": 10000
      }
    ],
    "debits": [
      {
        "name": "ทุนกู้ยืม",
        "amount": 25200
      }
    ],
    "total": {
      "name": "ชำระเงิน",
      "amount": 0
    },
    "payEnable": false
  }

  billNo:string = ''  

  public stateCommit$ = new BehaviorSubject<Commit>(this.initState)
  constructor() { }

  getvalue(): Commit{
    return this.stateCommit$.getValue()
  }

  getStateChange():Observable<Commit>{
    return this.stateCommit$.asObservable().pipe(
      tap(x => console.log(x)),
    )
    // return of(this.mockdata)
  }

  nexStateCommit(state: Commit){
    this.billNo = state.payInNo
    this.stateCommit$.next(state)
  }

  nexStateLogout(){
    this.stateCommit$.next(this.initState)
  }

}
