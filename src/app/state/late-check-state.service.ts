import { Injectable } from '@angular/core';
import { PreCheck } from '../shared/models/precheck';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LateCheckStateService {
  private initState: PreCheck =  {
    debits:[],
    credits:[],
    totalPaymentAmount: 0,
    minCredit: 0,
    maxCredit: 0,
    academicYear: 0,
    academicSemester: 0,
    isEligibleToEnroll: null,
    message: '',
    isReadOnly:null,
    currentCredit: 0,
    selectedSubjects:[]    
  }

  state$ = new BehaviorSubject<PreCheck>(this.initState)
  constructor() { }

  getValue():PreCheck {
    return this.state$.getValue()
  }

  getStateChange():Observable<PreCheck>{
    return this.state$.asObservable()
  }

  nextState(precheck: PreCheck){
    this.state$.next(precheck)
  }

  logoutState(){
    this.state$.next(this.initState)
  }
}
