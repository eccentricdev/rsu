import { Injectable } from '@angular/core';
import {  StudentProfile } from '../shared/models/profile-student';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileStudentStateService {
  private profileStudentState:StudentProfile = null
  private profileStudentState$ = new BehaviorSubject<StudentProfile>(this.profileStudentState);
  constructor() { }

  getStudentProfile():StudentProfile {
    return this.profileStudentState$.getValue()
  }

  getStudentProfileChange():Observable<StudentProfile> {
    return this.profileStudentState$
  }

  nextStateStudentProfile(studentProfile:StudentProfile){
    this.profileStudentState$.next(studentProfile)
  }

  nextStateLogout(){
    this.profileStudentState$.next(null)
  }

  

}
