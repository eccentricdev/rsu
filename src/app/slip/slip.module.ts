import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SlipComponent } from './slip.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

const route:Routes = [
  {
    path:'',
    component:SlipComponent
  }
]

@NgModule({
  declarations: [
    SlipComponent,    
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(route)
  ]
})
export class SlipModule { }
 