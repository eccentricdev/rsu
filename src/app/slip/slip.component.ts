import { Component, OnInit, ChangeDetectionStrategy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { StateCommitService } from '../state/commit-state.service';
import { ProfileStudentStateService } from '../state/profile-student-state.service';
import { combineLatest, Observable, throwError } from 'rxjs';
import { tap, switchMap, catchError } from 'rxjs/operators';
import { PrecheckStateService } from '../state/precheck-state.service';
import { PaymentPrecheckService } from '../core/services/payment/payment-precheck.service';
import { BillService } from '../core/services/payment/bill.service';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2'
import { splitMessage } from '../core/util/util';
import { PrecheckService } from '../core/services/precheck.service';
import { ifError } from 'assert';
import { LatePrecheckService } from '../core/services/late-precheck.service';
@Component({
  selector: 'app-slip',
  templateUrl: './slip.component.html',
  styleUrls: ['./slip.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class SlipComponent implements OnInit {
  imageBase64: string = ''
  imageBase642: string = ''
  isDisplayBarcode: boolean = false
  state$:Observable<any> 
  currentDate: String = new Date().toLocaleDateString()
  currentTime = new Date()
  sumCredit: number = 0
  sumDebit: number = 0
  constructor(
    public commitState:StateCommitService,
    public profileStudent: ProfileStudentStateService,
    public preCheckStateSV: PrecheckStateService,
    public PaymentPrecheckSV : PaymentPrecheckService,
    public BillSV : BillService,
    public router:Router,
    public activeRoute:ActivatedRoute,
    private precheckSV: PrecheckService,
    private lastPrecheckSV: LatePrecheckService
  ) { 
    
    let [d,m,y] = this.currentDate.split('/')
    let thaiYear = +y + 543
    this.currentDate = [d,m,thaiYear].join('/')
  }

  ngOnInit(): void {
    // this.precheckSV.getPreCheck().subscribe()
    
    this.state$ = combineLatest(
      this.commitState.getStateChange(),
      this.profileStudent.getStudentProfileChange(),
      this.preCheckStateSV.getStateChange().pipe(
        tap(x => {
          this.setDefault()
          if (x.credits.length != 0 && x.credits) {
            x.credits.forEach(x => this.sumCredit += x.amount)  
          }

          if(x.debits.length != 0 && x.debits){
            x.debits.forEach(x => this.sumDebit += x.amount)
          }
        })
      ),
      this.PaymentPrecheckSV.getPreCheckPayment().pipe(
        tap(x => {
          if (x.enrollmentSummary.type == '1') {
            this.precheckSV.getPreCheck().subscribe()
          } else {
            this.lastPrecheckSV.getPreCheck().subscribe()
          } 
        }),
        catchError(err => {
          this.router.navigate(['/rsu/home'])
          swal.fire({
            icon: 'error',
            html: splitMessage(err.error.message),
            // text: err.error.message,
          })
          return  throwError(err)
        })
      ),
      this.PaymentPrecheckSV.getPreCheckPayment().pipe(
        switchMap((x:any) => {
          return this.BillSV.billing(x.enrollmentSummary.payInNo).pipe(
            tap(x => console.log(x)),
            tap( (x:any) => {
              let base64 = 'data:image/png;base64,'
              this.imageBase64 = base64 + x.image
              this.imageBase642 = base64 + x.image2
            }),
            catchError(err => {
              this.router.navigate(['/rsu/home'])
              return throwError(err)
            })
          )
        }),
        catchError(err => {
          this.router.navigate(['/rsu/home'])
          swal.fire({
            icon: 'error',
            html: splitMessage(err.error.message),
            // text: err.error.message,
          })
          return  throwError(err)
        })
      )
      // this.BillSV.billing(this.commitState.getvalue().payInNo).pipe(
      //   tap(x => console.log(x)),
      //   tap( (x:any) => {
      //     let base64 = 'data:image/png;base64,'
      //     this.imageBase64 = base64 + x.image
      //   })
      // )
    ).pipe(
      tap(x => console.log(x))
    )
  }

  private setDefault(){
    this.sumCredit = 0
    this.sumDebit = 0
  }

  onBack(){
    this.router.navigate(['../'],{relativeTo: this.activeRoute})
  }

  

}
